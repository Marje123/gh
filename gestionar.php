<div id="w">
<div class="popover fade bottom in" style="top: 85px; left: 65%; display: block;">
            <div class="arrow" style="left: 10%;"></div>
            <div class="arrow" style="left: 90%;"></div>
            <h3 class="popover-title" style="text-align: center;">
                <strong>GESTIONAR</strong><br />
            </h3>
</div>

<div class="row-fluid">
    <div class="span3">
        <table class="table table-bordered">
            <thead>
                <tr><th><span class="titulo">Prueba</span></th></tr>
            </thead>
            <tbody>
                <tr>
	                <td style="text-align: center;">
                        <button id="btnMostrar" name="btnMostrar" class="btn btn-success">Mostrar</button>
	                </td>
                </tr>
            </tbody>
        </table>
        <div id="rsS"></div>
    </div>
    <div class="span9">
        <div id="rsS"></div>
    </div>
</div></div>



<script type="text/javascript">
    $(function() {

        $('#btnMostrar').click(function() {

                var datos = {
                    "m": "1",
                    "a": "2"
                }
                $.ajax({
                    data: datos,
                    url: 'validar.php',
                    type: 'post',
                    beforeSend: function() {
                        $("#rsS").html("<div id='resultado' class='btn-danger'>Procesando, espere por favor...</div>");
                    },
                    success: function(response) {
                        $("#rsS").html(response);
                    }
                });

        });

    });
</script>
