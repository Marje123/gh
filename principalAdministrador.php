<? session_start(); ?><!DOCTYPE html>
<html lang="es">
    <head>

  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html">
  <meta name="author" content="Jake Rocheleau">
  <link rel="shortcut icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="icon" href="http://d15dxvojnvxp1x.cloudfront.net/assets/favicon.ico">
  <link rel="stylesheet" type="text/css" media="all" href="css/styles.css">
  <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>




        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta charset="UTF-8" />
        <title>PRINCIPAL | Sistema de Gestion (Prueba)</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="favicon.ico" rel="shortcout icon" />

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <link rel="stylesheet" type="text/css" href="assets/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
        <link href="assets/css/app.css" rel="stylesheet" media="screen" />
    </head>
    <body >
        <div class="navbar">
            <div class="navbar-inner">
                <a class="brand"><img src="" width="150px" height="33px" alt="Sistema de Gestion (Prueba)"></a>
                <ul class="nav">
                    <li><a href="principalAdministrador.php">PRINCIPAL</a></li>
                    <li><a href="#" onclick="ver('gestionar.php');">GESTIONAR</a></li>
                    <li><a href="#" onclick="ver('githubtuto.php');">GITHUB TUTO</a></li>
                </ul>
            </div>
        </div>
        <div class="popover fade bottom in" style="top: 85px; left: 80%; display: block;">
            <div class="arrow" style="left: 10%;"></div>
            <div class="arrow" style="left: 90%;"></div>
            <h3 class="popover-title" style="text-align: center;">
                <strong>ADMINISTRADOR</strong><br />
                <br /><a href="login.php?logout" style="text-transform: uppercase; color: red">SALIR</a>
            </h3>
        </div>





        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <br />
                    <div class="row-fluid">
                        <div class="span12">
                            <div id="contenido"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/fancybox/jquery.fancybox.js?v=2.1.5"></script>
        <script type="text/javascript" src="assets/js/app.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.f').fancybox();
            });
        </script>
    </body>
</html>