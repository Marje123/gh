/* Validaciones */
(function(a) {
    a.fn.valSistemasIUJO = function(b) {
        a(this).on({keypress: function(a) {
                var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
                (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
            }})
    }
})(jQuery);

function ver(url) {
    $.ajax({
        url: url,
        beforeSend: function() {
            $("#contenido").html('<div id="cargando"><img src="assets/img/cargando2.gif" height="30" width="30" /><br />Cargando por favor espere.</div>');
        },
        success: function(response) {
            $("#contenido").html(response);
        }
    });
}