<!doctype html>
<html lang="en-US">

<div class="popover fade bottom in" style="top: 85px; left: 65%; display: block;">
            <div class="arrow" style="left: 10%;"></div>
            <div class="arrow" style="left: 90%;"></div>
            <h3 class="popover-title" style="text-align: center;">
                <strong>GITHUB TUTO</strong><br />
            </h3>
</div>

<body>
  <div id="w">


    <input type="text" name="txtusername" id="txtusername" value="" class="input-medium" placeholder="Nombre de Usuario"/><br />

    <button id="btnDatos" name="btnDatos" class="btn btn-success">Datos de Usuario</button>

    <div id="ghapidata" class="clearfix"></div>
  </div>



<script type="text/javascript">

$(function(){
  $('#btnDatos').on('click', function(e){
    e.preventDefault();
    $('#ghapidata').html('<div id="loader"><img src="css/loader.gif" alt="Cargando, espere por favor..."></div>');

    var nombreU = $('#txtusername').val();

    var requri   = 'https://api.github.com/users/'+nombreU;
    var repouri  = 'https://api.github.com/users/'+nombreU+'/repos';

    requestJSON(requri, function(json)
    {

      if(json.message == "Sin Resultados" || nombreU == '')
      {
        $('#ghapidata').html("<h2>Informacion no encontrada.</h2>");
      }

      else
      {

        var nombreC   = json.name;
        var nombreU   = json.login;
        var Img     = json.avatar_url;
        var urlP = json.html_url;
        var location   = json.location;
        var seguidores = json.followers;
        var siguiendo = json.following;
        var reposnum     = json.public_repos;

        if(nombreC == undefined)
          {
            nombreC = nombreU;
          }

        var outhtml = '<h2>'+nombreC+' <span class="smallname">(@<a href="'+urlP+'" target="_blank">'+nombreU+'</a>)</span></h2>';

        outhtml = outhtml + '<div class="ghcontent"><div class="avi"><a href="'+urlP+'" target="_blank"><img src="'+Img+'" width="80" height="80" alt="'+nombreU+'"></a></div>';

        outhtml = outhtml + '<p>Seguidores: '+seguidores+' - Siguiendo: '+siguiendo+'<br></p></div>';

        outhtml = outhtml + '<div class="repolist clearfix">';

        var repositories;
        $.getJSON(repouri, function(json){
          repositories = json;
          outputPageContent();
        });


        function outputPageContent()
        {
          if(repositories.length == 0)
            {
              outhtml = outhtml + '<p>Sin repositorios.</p></div>';
            }

          else
          {
            outhtml = outhtml + '<p><strong>Repositorios:'+reposnum+'</strong></p> <ul>';

            $.each(repositories, function(index)
            {
              outhtml = outhtml + '<li><a href="'+repositories[index].html_url+'" target="_blank">'+repositories[index].name + '</a></li>';
            });
            outhtml = outhtml + '</ul></div>';
          }

          $('#ghapidata').html(outhtml);
        }

      }

    });
  });


  function requestJSON(url, callback) {
    $.ajax({
      url: url,
      complete: function(xhr) {
        callback.call(null, xhr.responseJSON);
      }
    });
  }


});

</script>
</body>
</html>